# MonitorPanelSolar

Programa para Arduino Mega, para realizar el monitoreo de la producción de energía de los paneles solares.

El programa toma muestras cada 5 minutos y los guarda en una tarjeta SD en el direcotior /DATA/
El nombre de archivo es el dia mes y ano .csv Ej: 20102021.csv
El formato de archivo es un CSV (archivo separado por comas) con las siguientes columnas:
fecha temperatura[1-8] corriente[1-8] voltaje[1-8]

El programa toma su configuracion de "/CONFIG/conf" donde el formato es:
linea 1: SSID del AP para el wifi
linea 2: passwd del wifi
linea 3: intervalo en segundos de medicion
linea 4: intervalo en segundos de reintento en WIFI 
linea 5: habilitar wifi // "pendiente de implementacion"

Modulos de hardware
--------------------

**Sensor de corriente ACS712**

- Soporta hasta 5A
- No requiere libreria
- Tutorial de referencia: 
- NOTAS: Requiere cierta [calibración](https://docs.google.com/spreadsheets/d/1qFAuTepCf7Zk1P0qUpd-LJW_tiqsfLBA9UbcWIl54r8/edit?usp=sharing)


** Sensor de voltaje **

- Soporta hasta 25 volts.
- No requiere libreria
- Tutorial de referencia

** Modulo reloj DS3231**

- Soporta: 
- libreria: https://github.com/adafruit/RTClib
- Tutorial
- NOTA

** Modulo SD **

- Soporta
- libreria
- Tutorial
- NOTA

** Modulo WIFI ESP8266 **

Librerias necesarias
-----------------------
Se detallan a continuación las librerias necesarias para construir el proyecto con Arduino.

RTCLib de Adafruit version
OneWire by Jim Stud
DallasTemperature by Miles Burton
ITEADLib_Arduino_wee_8266 by Wu Pengfei
Arduino-Timer by Michael Contreras
