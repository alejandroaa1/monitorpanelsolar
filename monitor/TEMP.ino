#include <OneWire.h>
#include <DallasTemperature.h>

#define ONE_WIRE_BUS 4 // Se definel el pin de salida digital
#define SAMPLES_TEMP 10 

//Setup  para definir un oneWire para comunicarse con otro dispositivo oneWire
OneWire oneWire(ONE_WIRE_BUS);

//paso la referencia de la lib onewire a la lib de dalastemp
DallasTemperature sensors(&oneWire);

long acum_temp[TEMP_COUNT];     // acumulador para contador
//float temperature[TEMP_COUNT]; //salida  de los sensores de temperatura
//DeviceAddress sensors_id[4];

DeviceAddress tempDeviceAddress;

int deviceCount = 0;

DeviceAddress Probe01 = { 0x28, 0x51, 0x2A, 0x79, 0xA2, 0x00, 0x03, 0x22 };
DeviceAddress Probe02 = { 0x28, 0xF8, 0xA3, 0x79, 0xA2, 0x00, 0x03, 0xD0 };
DeviceAddress Probe03 = { 0x28, 0x9D, 0xA0, 0x79, 0xA2, 0x00, 0x03, 0xB6 };
DeviceAddress Probe04 = { 0x28, 0xBE, 0xF7, 0x79, 0xA2, 0x01, 0x03, 0x59 };
DeviceAddress Probe05 = { 0x28, 0xA6, 0xFF, 0x79, 0xA2, 0x01, 0x03, 0x9D };
DeviceAddress Probe06 = { 0x28, 0x60, 0xAD, 0x79, 0xA2, 0x00, 0x03, 0x62 };
DeviceAddress Probe07 = { 0x28, 0xAA, 0xBA, 0x9D, 0x3F, 0x14, 0x01, 0x0B };
//DeviceAddress Probe07 = { 0x28, 0x1D, 0xF7, 0x79, 0xA2, 0x01, 0x03, 0x5C };// old id
DeviceAddress Probe08 = { 0x28, 0x31, 0xE2, 0x79, 0xA2, 0x00, 0x03, 0xFD };


//DeviceAddress sensors_id[TEMP_COUNT] = {Probe01, Probe02, Probe03, Probe04};
void initTEMP(){
    
    sensors.begin();
    sensors.requestTemperatures(); 
    deviceCount = sensors.getDeviceCount();
    if (debug) {
      Serial.print("Device count: "); Serial.println(deviceCount);
    }

}

int checkTEMP(){
    //deviceCount = sensors.getDeviceCount(); // no siempre anda eso
    deviceCount = 0;
    for (int i=0;i<TEMP_COUNT;i++) {
      if (temperature[0]== -127) {
        if (debug) {
          Serial.print("Error en sensor: ");
          Serial.println(i);
        }
        writeLog(ERROR, "Error en sensor " + i);
        return 1; // un valor fue -127, valor que corresponde a no lectura
      }
    }
    return 0; //no hay error

}

void getTEMP(){
  //Pido las temperaturas a los sensores
  sensors.requestTemperatures();
  // tomar muestras de todos los sensores de temperatura una para cada sensor
  temperature[0]=sensors.getTempC(Probe01);
  temperature[1]=sensors.getTempC(Probe02);
  temperature[2]=sensors.getTempC(Probe03);
  temperature[3]=sensors.getTempC(Probe04);
  temperature[4]=sensors.getTempC(Probe05);
  temperature[5]=sensors.getTempC(Probe06);
  temperature[6]=sensors.getTempC(Probe07);
  temperature[7]=sensors.getTempC(Probe08);
}
