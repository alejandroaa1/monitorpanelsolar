/**********************************************************************************
 *  
 *  Esta seccion del codigo se encarga de realizar las lecturas con el sensor de
 *  corriente ACS712 y de voltaje 
 *  
 *  
 **********************************************************************************/




#define SAMPLES_CURR 900 // numero de muestras para el sensor de corriente
#define SAMPLES_VOLT 100   // numero de muestras para el sensor de voltaje
#define VOLT_SLOPE  0.023936 // ajuste para el valor del sensor de vooltaje
#define VOLT_OFFSET -0.326293

//Entrada                  1   2   3   4     5   6    7    8
int pinVOLT[VOLT_COUNT] = {A0, A4, A8, A12,  A1 ,A5,  A9,  A13};           // pin de sensores de voltaje
int pinCURR[CURR_COUNT] = {A2, A6, A10, A14, A3, A7, A11, A15};           // pin de sensores de corriente

float offsetZERO[CURR_COUNT] = {
  -18.510738,
  -13.440972,
  -13.392086,
  -18.925582,
  -13.687813,
  -13.658656,
  -18.512296,
  -18.578614
};

float offsetSLOPE[CURR_COUNT] = {
  0.036278,
  0.026055,
  0.026035,
  0.036664,
  0.026581,
  0.026571,
  0.035931,
  0.036040
};


void initVOLT() {
  Serial.println("Inicializando sensores de Voltaje");
  
  // modo input los pins de los sensores de voltaje
  for (int sensor=0; sensor<VOLT_COUNT; sensor++) {
    pinMode(INPUT,pinVOLT[sensor]);
  }
}

void initCUR() {
  Serial.println("Inicializando sensores de Corriente");
  
  // modo input los pins de los sensores de corriente
  for (int sensor=0; sensor<CURR_COUNT; sensor++) {
    pinMode(INPUT,pinCURR[sensor]);
  }

}


int checkCUR() {
  for (int sensor=0; sensor<CURR_COUNT; sensor++) {
      int reading = analogRead(pinCURR[sensor]);
      if (reading <300 || reading >= 1023) { // si hay corriente negativa o demasiada alt
        Serial.print("error: en sensor de corriente:");
        Serial.println(sensor+1);
        return 1;
      };
  }
  return 0; // no hay error
}

int checkVOLT() {
  for (int sensor=0; sensor<VOLT_COUNT; sensor++) {
      int reading = analogRead(pinVOLT[sensor]);
      if (reading >= 1023) { // si hay un voltaje demasiado alto
        Serial.print("error: en sensor de voltaje:");
        Serial.println(sensor+1);
        return 1;
      };
  }
  return 0; // no hay error
}

// qsort requires you to create a sort function
int sort_desc(const void *cmp1, const void *cmp2)
{
  // Need to cast the void * to int *
  int a = *((int *)cmp1);
  int b = *((int *)cmp2);
  // The comparison
  return a > b ? -1 : (a < b ? 1 : 0);
  // A simpler, probably faster way:
  //return b - a;
}

float getCurFromSensor(int sensor) { // obtiene el valor RAW del sensor de corriente y lo ajusta
  int recorte = 200; // min/max
  int rawData[SAMPLES_CURR];
  float acum = 0;
  float avg;
  
  for (int i=0;i<SAMPLES_CURR;i++) { // tomar N muestras para el sensor
      rawData[i] = analogRead(pinCURR[sensor]); 
  }

  qsort(rawData, SAMPLES_CURR, sizeof(rawData[0]), sort_desc);    
    
    
  for (int i=recorte;i<SAMPLES_CURR-recorte;i++) {
    acum += rawData[i];
  }
  
  avg = acum / (SAMPLES_CURR-recorte*2);
  currentRaw[sensor]= avg;
 
  return max(offsetSLOPE[sensor]*avg + offsetZERO[sensor], 0); // ajuste por caracterizacion de sensor
}

void getCUR() { // actualiza las lecturas del sensor de corriente
  for (int sensor=0; sensor<CURR_COUNT; sensor++) { 
    current[sensor] = getCurFromSensor(sensor);
  }
}

float getVoltFromSensor(int sensor, bool rawdata = false) {// obtiene el valor RAW del sensor de voltaje y lo ajusta
  float acum = 0;
  float avg; 
  for (int i=0;i<SAMPLES_VOLT;i++) {
    acum+=analogRead(pinVOLT[sensor]); // acumular el valor leido
  }
  avg = acum / SAMPLES_VOLT;
  voltageRaw[sensor] = avg;
  
  return max(VOLT_SLOPE * avg + VOLT_OFFSET, 0);
}
  
void getVOLT() { // actualiza las mediciones de voltaje
  for (int sensor=0; sensor<VOLT_COUNT; sensor++) {
    voltage[sensor] = getVoltFromSensor(sensor);
  } 
}
