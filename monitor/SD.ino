/*
 * IMPORTANTE PARA VERSIONES ANTERIORES DE LA LIB ANTERIOR A: 1.2.3
 * Modificar el archivo SD.cpp
 * Y agregar en begin la linea: 
 * if (root.isOpen()) root.close();      // allows repeated calls <- 
 * referencia: https://forum.arduino.cc/index.php?topic=46969.0
 */

#include <SD.h>

int pinCS = 53; //pin para verificar funcionamiento de la memoria

String logAux = "";
String sep = ","; //separador
bool statusSD = false;
int logCount = 0; //contador de logs acumulados
String configFileName = "/CONFIG/conf";
String logFileName= "/log.txt";

void initSD(){
    pinMode(pinCS,OUTPUT);
    Serial.println("Inicializado SD");
    statusSD = SD.begin(pinCS);
    if (debug) {
      if (!statusSD) {
        Serial.println("Error inicializando SD");
      }
    }
    if (!SD.exists("data")) {
      if (debug) {
        Serial.println("Carpeta de datos no existente");
      }
      SD.mkdir("data");
    }

}

int checkSD(){
    if (statusSD) {
        return 0;
    }
    return 1; //hay error
}

String generateFileName(String ddate) {
  return "/DATA/" + ddate +".csv";
};


void WriteData(){
  statusSD = SD.begin(pinCS);
  
  now_clock = get_time(); // Mario

  logAux += now_clock + sep; // se agrega la fecha
  //TEMPERATURA
  for (int i=0;i<TEMP_COUNT;i++) {
    logAux += String(temperature[i]) + sep;
  }
  //CORRIENTE
  for (int i=0;i<CURR_COUNT;i++) {
    logAux += String(current[i]) + sep;
  }
  //VOLTAJE
  for (int i=0;i<VOLT_COUNT;i++) {
     logAux += String(voltage[i]) + sep;
  }
  //CORRIENTE_RAW
  for (int i=0;i<CURR_COUNT;i++) {
    logAux += String(currentRaw[i]) + sep;
  }
  //VOLTAJE RAW
  for (int i=0;i<VOLT_COUNT;i++) {
    logAux += String(voltageRaw[i]) + sep;
  }
 
  logAux += "\n"; // fin de linea

  logCount+=1; // se acumulan 1 lectura 
  
  if (statusSD) { // si la SD esta disponible
    String fname;
    File logFile;
    
    fname = generateFileName(get_date());
    
    if (debug) {
      Serial.println("Nombre de archivo: "+ fname);
    }
    
    logFile = SD.open(fname, FILE_WRITE); // se abre el archivo
    
    if(logFile){ // si se pudo abrir el archivo
      logFile.print(logAux); // se imprime el log
      if (debug) {
        Serial.println("info: Se guardo en el archivo");
        Serial.println(logAux);
      }
      logAux=""; // se vacia el buffer
      logCount = 0; // se vuelve a 0
      logFile.close(); // se cierra el archivo
    } else { // si no pudo guardar el archivo
      if (debug) {// si no se pudo guardar, ocurrio un error
        globalErrors+=1;
        Serial.println("Error: No se pudo abrir el archivo");
        Serial.print("info: logs acumulados "); Serial.println(logCount);
      }
    }    
   } else {
      // error en la inicializacion
      if (debug) {
        globalErrors+=1;
        Serial.println("Error: no se pudo acceder a la SD");
        Serial.print("info: logs acumulados"); Serial.println(logCount);
        Serial.println(logAux);
      }
    }
}

void writeLog(int type, String s) {
  if (!logEnable) return;
  
  File logFile;
  logFile = SD.open(logFileName,FILE_WRITE); 
  if (logFile) {
    now_clock = get_time(); 
    logFile.print(now_clock + " ");
    
    switch (type) {
      case ERROR:
        logFile.print("ERROR: ");
      break;
      case INFO:
        logFile.print("INFO: ");
      break;
    }
    logFile.println(s);
    logFile.close();
  }

  Serial.println(s);
}

void loadConfig() {
  statusSD = SD.begin(pinCS); 
  if (debug) {
    Serial.println("Buscando archivo de configuracion");
  }
  if (statusSD) { // si esta operativa la SD
    if (!SD.exists(configFileName)) { 
     if (debug) {
      Serial.println("No existe archivo de configuracion");
     }
     return; // si no hay archivo de configuracion, hace skip
    }
    
    File myFile;
    myFile = SD.open(configFileName,FILE_READ); // abre el archivo de configuracion

    if (debug) {
      Serial.println("Archivo de configuracion encontrado");
    }

    int lineNumber = 0;
    String line = "";
    
    while (myFile.available()) { // leer linea a linea      
      char last_char = myFile.read(); 
      line += last_char;
      if (last_char=='\n') {
        lineNumber+=1;
      }
      while (myFile.available() && last_char !='\n') {
        last_char = myFile.read(); 
        if (last_char!='\n') {
          line += last_char;       
        } else {
          lineNumber+=1;
        }
      }
      // fin de linea o archivo
      
      if (lineNumber==1) { // linea 1 es ssid
        ssid = line;
        if (debug) {
          Serial.println("SSID: " + ssid);
        }
      }
      if (lineNumber==2) { // linea 2 es passwd
        passwd = line;
        if (debug) {
          Serial.println("passwd: " + passwd);
        }
      }
      if (lineNumber==3) { // linea 3 es intervalo de medicion
        dataInterval = line.toInt()*1000;
        if (debug) {
          Serial.print("Intervalo de medicion");
          Serial.println(dataInterval);
        }
      }
      if (lineNumber==4) { // linea 4 es intervalo de reintento
        wifiInterval = line.toInt()*1000;
        if (debug) {
          Serial.print("Intervalo de reintento");
          Serial.println(wifiInterval);
        }
      }
      line = "";
    }
    Serial.println(line);
    myFile.close(); //cerramos el archivo
  }
}
