/*
#include "ESP8266.h"
#include <SoftwareSerial.h>
#define LISTENING 0
#define ANSWERING 1
#define SENDING   2
#define DEAD      3
//        

int error = 0;
int stateWIFI = LISTENING;
uint32_t len=0;

uint8_t buffer[128] = { 0 };
uint8_t mux_id;

String toPrintDay = "";
String toPrintMonth = "";
String toPrintYear = "";

ESP8266 wifi(Serial1);

void initWIFI(String _ssid, String _passwd) {
   stateWIFI = LISTENING;
   wifi.restart();
   delay(500);
   int error = 0;
   if (wifi.setOprToStationSoftAP()) {
      if (debug) Serial.print("to station + softap ok\r\n");
   } else {
      if (debug) Serial.print("to station + softap err\r\n");
      error+=1;
   }
   if (passwd="") {
    if (debug) {
      Serial.println("Password vacia");
    }
     if (wifi.joinAP(ssid, passwd)) { //todo conexion sn passwd
        if (debug) {
          Serial.print("Join AP success\r\n");
          Serial.print("IP: ");
          Serial.println(wifi.getLocalIP().c_str());
        }
     } else {
        if (debug) Serial.print("Join AP failure\r\n");
        error+=1;
     }
   } else {
     if (wifi.joinAP(ssid, passwd)) {
        if (debug) {
          Serial.print("Join AP success\r\n");
          Serial.print("IP: ");
          Serial.println(wifi.getLocalIP().c_str());
        }
     } else {
        if (debug) Serial.print("Join AP failure\r\n");
        error+=1;
     }
   }

   if (wifi.enableMUX()) {
      if (debug) Serial.print("multiple ok\r\n");
   } else {
      error +=1;
      if (debug) Serial.print("multiple err\r\n");
   }

   if (wifi.startTCPServer(80)) {
      if (debug) Serial.print("start tcp server ok\r\n");
   } else {
      if (debug) Serial.print("start tcp server err\r\n");
      error+=1;
   }

   if (wifi.setTCPServerTimeout(10)) {
      if (debug) Serial.print("set tcp server timout 10 seconds\r\n");
   }  else {
      if (debug) Serial.print("set tcp server timout err\r\n");
      error+=1;
   }

   if (debug) Serial.println("setup end\r\n");
}

int checkWIFI() {
  if (!wifiEnabled) return 0;
  error = 0;
  if (!wifi.kick()) {
    error+=1;
    if (debug) Serial.println("Wifi module lost");
  }
  String rst= wifi.getLocalIP().c_str();
  if (rst.indexOf("+CIFSR:APMAC")<0) {
    error+=1;
    if (debug) Serial.println("Wifi connection lost");
  } 
  return error;
}

#define wifiWrite(A) wifi.send(mux_id, (uint8_t*) A, sizeof(A) - 1);

void manageWIFI() {
  switch (stateWIFI) {
    case LISTENING:
      //Serial.println("State: LISTENING");
      len = wifi.recv(&mux_id, buffer, sizeof(buffer), 100);
      if (len > 0) {
        if (debug) {
          Serial.print("Received from: ");
          Serial.print(mux_id);
          Serial.print("\r\n");
          for (uint32_t i = 0; i < len; i++) {
            Serial.print((char)buffer[i]);
          }
          Serial.print("\r\n");
        }
        stateWIFI = ANSWERING;
        Serial.print("State:");
        Serial.println(stateWIFI);
      }
      break;
    case ANSWERING:
      {
        //Serial.print("State: ANSWERING");
        bool getFile= false;
        String bufferString = buffer;
        String str ="";
  
        int param = bufferString.indexOf("?");
        if (param>0) {
          getFile = true;
          str = bufferString.substring(param);
          String _fromDay = str.substring(15,17);
          String _fromMonth = str.substring(12,14);
          String _fromYear = str.substring(7,11);
          
          String _toDay = str.substring(30,32);
          String _toMonth = str.substring(27,29);
          String _toYear = str.substring(22,26);
      
          toPrintDay = _fromDay;
          toPrintMonth = _fromMonth;
          toPrintYear = _fromYear;
                
          if (debug) Serial.println("From:" + _fromDay + _fromMonth + _fromYear + "to :" + _toDay + _toMonth + _toYear);
        }
        
  
        if (!getFile) { // no se pidio ningun archivo se devuelve el home
          printHome();
          closeConnection();
          stateWIFI = LISTENING;
        } else { // si se pidio se pasa al estado SENDING
          stateWIFI = SENDING;
          bool hasPrintedDate =   printDate(toPrintDay,toPrintMonth,toPrintYear);
          if (!hasPrintedDate) {
            wifiWrite("HTTP/1.1 404\r\nnContent-Type: text/plain\r\nConnection: close\r\n\r\n");
          }
          closeConnection();
        }
      }
      break;
    case SENDING:
    break;
  }  
}
  
void printHome() {
  if (debug) Serial.println("Imprime home");
  wifiWrite("HTTP/1.1 200 OK\r\nnContent-Type: /html\r\nConnection: close\r\n\r\n");
  wifiWrite("<html><head> <title>Monitor Gersolar</title></head><style> input[type=submit] { padding:5px 15px; background:#003366; border: none;color: white; border:0 none; cursor:pointer; -webkit-border-radius: 5px; border-radius: 5px; }.content { padding-top: 50px; padding-right: 30px; padding-bottom: 50px; padding-left: 80px; max-width: 80%; margin: auto; background:#DFEEEE}a:link { text-decoration: none; color: #003366; }a:visited { text-decoration: none; color: #003366; }a:hover { text-decoration: underline;}a:active { text-decoration: underline;}</style><div class=\"content\"><body><p>&nbsp;</p><h2><span style=\"background-color: #003366; color: #ffffff;\">Monitor Gersolar v1</span></h2><p> Software de monitoreo de los paneles solares. Codigo fuente y documentacion disponible <a href=\"https://gitlab.com/alejandroaa1/monitorpanelsolar\">aqui.</a> </p><form action=\"./download\" method=\"get\" target=\"_blank\"><p>Desde <input id=\"dfrom\" name=\"dfrom\" type=\"date\" value=\"2021-10-21\" /> <input type=\"submit\" value=\"Descargar muestras\" /></p></form><h5 style=\"text-align: right;\">Powered by <a href=\"https://cidetic.unlu.edu.ar\">CIDETIC</a></h5><p>&nbsp;</p><div></body></html>");
};

bool printDate(String dday,String dmonth, String dyear)  {
  File myFile;
  String fname = generateFileName(dday + dmonth + dyear);
  Serial.println("Se busca el archivo");
  Serial.println(fname);
  
  statusSD = SD.begin(pinCS);
  if (!statusSD) {
    if (debug) {
      Serial.println("Error leyendo la tarjeta SD");
    }
    return false;
  }
    
  if (SD.exists(fname)) {
    myFile = SD.open(fname, FILE_READ); // se abre el archivo
    char line[256] = "";
    char fnameDownload[20];
    String dname = dday + dmonth + dyear + ".csv";
    dname.toCharArray(fnameDownload,20);
    sprintf(line,"HTTP/1.1 200 OK\r\n nContent-Type: /text\r\nContent-Disposition: attachment; filename=\"%s\"\r\nConnection: close\r\n\r\n",fnameDownload);   
    wifiWrite(line);    
    Serial.println("Se va a leer el archivo");  
    while (myFile.available()) { // leer linea a linea
      char last_char = myFile.read();
      char bufferSend[256];
      int currentLen=0;
      
      //line += last_char;
      bufferSend[currentLen] = last_char;
      currentLen+=1;
      while (myFile.available() && last_char !='\n') {
        last_char = myFile.read();
        bufferSend[currentLen] = last_char;
        currentLen+=1;
        //line += last_char;       
      }
       
      if (currentLen>1) {
        wifi.send(mux_id, bufferSend,currentLen);
      }
      //line = "";
    }
    myFile.close(); //cerramos el archivo
    return true;
  } else {
    if (debug) {
      Serial.println("No existen datos para ese archivo " + fname);
    }
    return false;
  }
}

void closeConnection() {
    if (wifi.releaseTCP(mux_id)) {
       if (debug) Serial.print("release tcp ");
       if (debug) Serial.print(mux_id);
       if (debug) Serial.println(" ok");
    } else {
       if (debug) Serial.print("release tcp");
       if (debug) Serial.print(mux_id);
       if (debug) Serial.println(" err");
    }
    stateWIFI = LISTENING;
}
*/
