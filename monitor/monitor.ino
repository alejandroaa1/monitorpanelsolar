#include <arduino-timer.h>


String now_clock="";
bool debug = true; // Permite ver las salidas en el programa con Serial.debug
bool logEnable = true; // Permite loggear eventos en un archivo

bool outOfDateLogged = false; // Determina si se notifico el evento "perder hora"

#define ERROR 0
#define INFO 1
#define TEMP_COUNT 8  // cantidad de sensores de temperatura
#define CURR_COUNT 8      // cantidad de sensores de corriente
#define VOLT_COUNT 8      // cantidad de sensores de voltaje
#define ERROR_PIN 8 // pin de LED de error

float temperature[TEMP_COUNT]; // Salida de los sensores de temperatura
float current[CURR_COUNT]; // salida de los sensores de corriente medida en Ampers
float voltage[VOLT_COUNT]; // salida de los sensores de voltaje
float voltageRaw[VOLT_COUNT]; // salida de los sensores de voltaje
float currentRaw[CURR_COUNT]; // salida de los sensores de corriente medida en Ampers

int globalErrors = 0;

auto timer = timer_create_default(); // crea un timer para luego disparar los evento de escritura yreintento wifi
bool wifiEnabled = false; // si esta en true, se habilita el uso del WIFI

unsigned long int dataInterval = (unsigned long) 1000*60*5; // 5 minutos, esta en milisegundos <- ojo toma de archivo de configuracion luego
unsigned long int wifiInterval = (unsigned long) 1000*60*2; // 2 minutos, esta en milisegundos
unsigned long int logInterval = (unsigned long) 1000*60*5; // 5 minutos, esta en milisegundos

String ssid = "SSID";
String passwd = "PASSSWD";

void setup() {
  if (debug)  {
    Serial.begin(9600); // Conexion Serial para mostrar debug
  }
  
  Serial.println("Espere 2 segundos");
  delay(2000); // se espera para estabilizar lecturas y posible iniciacion de sensores
  pinMode(ERROR_PIN, OUTPUT); // pin para indicar errores
  Serial.println("Inicializacion de sub-sistemas");
  
  initSD(); // Inicializar SD Agus
  delay(1000); 
  
  initCLOCK(); // Inicializar Reloj Mario
  delay(500);

  //set_time(25, 03,2022,14,34,00);// <- poner en hora si se acaba la pila
  //updateTimeFromCompiler();

  loadConfig(); // cargar configuracion de archivo
  //dataInterval = 10000;  // sobreescribe el tiempo de muestreo
  delay(500);
  
  initTEMP(); // Iniciar sensores de temperatura Agus
  delay(500);
  
  initCUR(); // Iniciar sensores de corriente Ale
  delay(500);
  
  initVOLT(); // Iniciar sensores de voltaje Ale
  delay(500);
  
  if (wifiEnabled) { // si esta el wifi habilitado
   // initWIFI(ssid,passwd); // Iniciar modulo WIFI
  }
  //delay(1000);

  Serial.println("Ejecutando pruebas del sistema");
  // Los check devuelven un INT que es un codigo de error
 
  if (checkSD()>0) { // Agus
    globalErrors+=1;
    Serial.println("--- SD Fail");
  } else {
    Serial.println("--- SD Ok");
  } 
  
  if (checkCLOCK()>0) { // Mario
    globalErrors+=1;
    Serial.println("--- Reloj fail");
  } else {
    Serial.println("--- Reloj ok");
  }

  if (checkCUR()>0) { // Ale
    globalErrors+=1;
    writeLog(ERROR ,"--- Sensor Cur fail");
  } else {
    writeLog(INFO,"--- Sensor Curr ok");
  }
  
  if (checkVOLT()>0) { // Ale
    globalErrors+=1;
    writeLog(ERROR, "--- Sensor Volt fail");
  } else {
    writeLog(INFO, "--- Sensor Volt ok");
  }

  getTEMP(); // check temp necesita al menos una lectura
  if (checkTEMP()>0) { // Agus
    globalErrors+=1;
    writeLog(ERROR, "--- Sensor Temp fail");
  } else {
    writeLog(INFO, "--- Sensor Temp ok");
  }
  /*if (wifiEnabled) { // SOLO MODULO WIFI
    if (checkWIFI()>0) { // Ale 
      globalErrors+=1;
      writeLog(ERROR, "--- Wifi Module fail");
    } else {
      writeLog(INFO, "--- Wifi Module ok");
    }
  }*/
  
  writeLog(INFO,"Sistema iniciado a "+get_time());

  timer.every(dataInterval, dataWriting); // el timer para escrbir las lecturas en la SD
  //timer.every(wifiInterval, wifiRetry); // el timer para verificar y reintentar la conexion WIFI // SOLO MODULO WIFI
  timer.every(logInterval, logWriting);

  writeLog(INFO, "Registrando datos cada " + String(logInterval/1000) + " segundos");

  digitalWrite(ERROR_PIN, LOW);
  if (globalErrors>0) {
    digitalWrite(ERROR_PIN, HIGH); // se muestra que hay problemas con el led
    delay(1000);
    globalErrors = 0;
  }
}

/* SOLO WIFI
bool wifiRetry(void *) { // se comprueba la conexion WIFI y se reintenta conectar
  if (wifiEnabled) {
    if (debug) {
      Serial.println("WIFI CHECK");
    }
    if (checkWIFI() > 0) { // si hay un error inteta reconectar
      writeLog(INFO, "Intento de reconeccion en WIFI");
      Serial.println("WIFI reconnect");
      initWIFI(ssid,passwd); // Ale
    }
  };
  
  return true; // true para que se siga ejecutando 
}*/

bool dataWriting(void *) {// se escriben los datos de lectura en la SD
  // Toma los datos
  getTEMP(); // Agus
  getCUR(); // Ale
  getVOLT(); // Ale

  // Escribe los datos
  WriteData(); // Agus
  
  return true; // true para que se siga ejecutando 
}

bool logWriting(void *) {
  writeLog(INFO,"Chequeo programado cada 5 minutos");
  if (outOfDate() && !outOfDateLogged) {
    outOfDateLogged = true;
    writeLog(ERROR, "Fuera de fecha");
  }
  checkTEMP(); // escribe solo en en log
  /*if (wifiEnabled && checkWIFI()>0) { SOLO WIFI
    writeLog(ERROR, "WIFI check fail");
  }*/ 
  return true;
}

void loop() {
  timer.tick(); // incrementa los contadores para los eventos de escribir y chequear wifi
  /*if (wifiEnabled) { SOLO WIFI
    manageWIFI(); // gestiona el WIFI si corresponde
  }*/
  
  if (globalErrors>0) {
    digitalWrite(ERROR_PIN, HIGH);
    globalErrors = 0;
  } else {
    digitalWrite(ERROR_PIN, LOW);
  }
}
