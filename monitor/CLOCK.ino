#include <Wire.h>
#include "RTClib.h"  // https://github.com/adafruit/RTClib
#include <TimeLib.h>
 
RTC_DS3231 rtc;

void set_time(int day, int month, int year, int hour, int min, int second) // ajusta la hora del reloj
{
  rtc.adjust(DateTime(year, month, day, hour, min, second)); 
}

void updateTimeFromCompiler() {
  char const *date = __DATE__;
  char const *time = __TIME__;
  char s_month[5];
  int year, day, month;
  int hour, minute, second;
  
  static const char month_names[] = "JanFebMarAprMayJunJulAugSepOctNovDec";
  sscanf(date, "%s %d %d", s_month, &day, &year);
  //sscanf(time, "%2hhd %*c %2hhd %*c %2hhd", &t.Hour, &t.Minute, &t.Second);
  sscanf(time, "%2d %*c %2d %*c %2d", &hour, &minute, &second);
  // Find where is s_month in month_names. Deduce month value.
  month = (strstr(month_names, s_month) - month_names) / 3 + 1;

  set_time(day, month, year, hour, minute, second);
}


 
String get_time() // devuelve un string co la hora y fecha
{
  DateTime now = rtc.now();
  char s[22] ={0};
  sprintf(s, "%02d:%02d:%02d %02d/%02d/%02d",  now.hour(), now.minute(), now.second(), now.day(), now.month(), now.year());
  String res = String(s);

  return res;
}

String get_date() { // devuelve la fecha sola
  DateTime now = rtc.now();
  char s[22] ={0};
  sprintf(s, "%02d%02d%02d", now.day(), now.month(), now.year());
  String res = String(s);
  return res;
}

void initCLOCK() // iniciliaza el reloj
{
  Serial.println("Inicializando reloj");
  if (!rtc.begin()) {
        Serial.println(F("Couldn't find RTC"));
        Serial.println("No se pude funcionar sin reloj");
         while (true) {// si no hay reloj no se puede funcionar
            delay(500);
            digitalWrite(ERROR_PIN, HIGH);           
            delay(500);
            digitalWrite(ERROR_PIN, LOW);
         }
   } 
}

bool outOfDate() {
  DateTime now = rtc.now();
  return (now.year() == 2000);
}

int checkCLOCK(){
    // Determina si se ha perdido la corriente
    if (rtc.lostPower()) {
        Serial.println(F("Reloj lostPower"));
        return 1; 
    }
    return 0; // No hay error
}
